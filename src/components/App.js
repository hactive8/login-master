import React, { Fragment, Component } from 'react';
import { Image } from 'react-bootstrap';
import LoginBox from './LoginBox';

class App extends Component {
  render() {
    return (
      <Fragment>
        <Image src="https://backgrounddownload.com/wp-content/uploads/2018/09/electronics-vector-background-9.jpg" style={{ position: 'absolute', height: '100%', width: '100%' }}></Image>
        <LoginBox />
      </Fragment>
    )
  }
};

export default App;
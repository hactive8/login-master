import React, { Component } from 'react';
import { Card, Form, Button, Alert } from 'react-bootstrap';
import logo from '../assets/logo.png';
import { Image } from 'react-bootstrap';
import { Login } from '../actions/AuthActions';
import { connect } from 'react-redux';

class LoginBox extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            alertShow: false
        }
    }

    onEmailChanged = (e) => {
        this.setState({ email: e.target.value });
    }

    onPasswordChanged = (e) => {
        this.setState({ password: e.target.value });
    }

    showAlert(isShow = false) {
        this.setState({ alertShow: isShow });
    }


    render() {
        const { loginTitle, loginMessage, loginIsValid, Login } = this.props;
        return (
            <Card style={{ width: '25em', flex: 1, position: 'absolute', top: '20%', left: '35%' }}>
                <Card.Body>
                    <center>
                        <Image src={logo} width={'100%'} />
                        <h3 style={{ color: 'grey' }}>Login</h3>
                        {
                            (this.state.alertShow) ? (
                                <Alert style={{ textAlign: 'left' }} variant={loginIsValid ? "success" : "danger"} onClose={() => this.showAlert(false)} dismissible>
                                    <Alert.Heading>{loginTitle}</Alert.Heading>
                                    <p>
                                        {loginMessage}
                                    </p>
                                </Alert>) : null
                        }
                    </center>
                    <Form onSubmit={(e) => { e.preventDefault(); Login(this.state.email, this.state.password); this.showAlert(true); }}>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control value={this.state.email} type="email" placeholder="Enter email" required="required" onChange={this.onEmailChanged} />
                            <Form.Text className="text-muted">

                            </Form.Text>
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control value={this.state.password} type="password" placeholder="Password" required="required" onChange={this.onPasswordChanged} />
                        </Form.Group>
                        <Button variant="info" style={{ width: '100%' }} type="submit">
                            Login
                        </Button>
                    </Form>
                </Card.Body>
            </Card >

        )
    }

};

const mapStateToProps = state => ({ loginIsValid: state.auth.loginIsValid, loginMessage: state.auth.loginMessage, loginTitle: state.auth.loginTitle });
const mapDispatchToProps = dispatch => ({
    Login: (email, password) => {
        dispatch(Login(email, password));
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(LoginBox);
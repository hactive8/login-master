const staticLogin = (inputEmail, inputPassword) => {

    let storedUsers = [
        { email: 'birin243@hacktiv8.com', password: 'P@ssw0rd' },
        { email: 'birin@hacktiv8.com', password: 'b1121n' },
    ];

    let isValid = storedUsers.filter(({ email, password }) => email === inputEmail && password === inputPassword).length > 0;

    return (isValid) ? {
        loginIsValid: true,
        loginTitle: 'Login Success!',
        loginMessage: 'Your Login Credential is Valid!'
    } : {
            loginIsValid: false,
            loginTitle: 'Login Failed!',
            loginMessage: 'Login was failed, wrong email or password!'
        };

}

const AuthReducer = (state = [], action) => {
    switch (action.type) {
        case 'LOGIN': {
            return staticLogin(action.email, action.password);
        }
        default: {
            return state;
        }
    }
}

export default AuthReducer;